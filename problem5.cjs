const problem5 = (inventory) => {
  let result = [];
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].car_year < 2000) {
      result.push(inventory[index]);
    }
  }
  return result;
};

module.exports = problem5;
