const problem6 = (inventory) => {
  let result = [];
  for (let index = 0; index < inventory.length; index++) {
    if (
      inventory[index].car_make === "Audi" ||
      inventory[index].car_make === "BMW"
    ) {
      result.push(inventory[index]);
    }
  }
  return result;
};

module.exports = problem6;
