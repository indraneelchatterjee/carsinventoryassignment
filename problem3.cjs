const problem3 = (inventory) => {
  let sortArr = inventory;
  sortArr.sort((a, b) =>
    a.car_model > b.car_model ? 1 : b.car_model > a.car_model ? -1 : 0
  );
  return inventory;
};

module.exports = problem3;
