const problem1 = require("../problem1.cjs");
const inventory = require("../cars.cjs");

console.log(problem1());
console.log(problem1(inventory));
console.log(problem1(33, inventory));
console.log(problem1({}, 33));
console.log(problem1(inventory, 33));
console.log(problem1([]));
console.log(problem1(new String("hello"), 33));
console.log(problem1(inventory, []));
console.log(problem1({ id: 33, name: "Test", length: 10 }, 33));
console.log(
  `Car 33 is a ${problem1(inventory, 33).car_year} ${
    problem1(inventory, 33).car_make
  } ${problem1(inventory, 33).car_model}`
);
