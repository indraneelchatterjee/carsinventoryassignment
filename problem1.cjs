const problem1 = (inventory, carId) => {
  if (!inventory) {
    return [];
  }
  if (typeof carId !== "number") {
    return [];
  }
  if (Array.isArray(inventory) == false) {
    return [];
  }
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].id === carId) {
      return inventory[index];
    }
  }
};
module.exports = problem1;
