const problem4 = (inventory) => {
  let result = [];
  for (let index = 0; index < inventory.length; index++) {
    result.push(inventory[index].car_year);
  }
  return result;
};

module.exports = problem4;
